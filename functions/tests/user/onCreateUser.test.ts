import { User, UserDbInterface } from '../../src/user/onCreateUser';
import { createMock } from "ts-auto-mock";
import { method, On } from 'ts-auto-mock/extension';

describe('onCreateUser', function() {
  let mock: UserDbInterface;

  it('firestore creates user with success', function() {
    const authUser = { email: "teste@user.com"};
    mock = createMock<UserDbInterface>();
    console.log(mock);
    const user = new User(authUser.email, mock);
    const saveMethod: jest.Mock = On(mock).get(method('save'));
    user.save();

    expect(saveMethod).toHaveBeenCalled();
  });
});
