import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { onCreateUser } from './user/onCreateUser';

admin.initializeApp();

export const addMessage = functions.https.onRequest(async (req, res) => {
  const original = req.query.text;
  const snapshot = await admin.database().ref('/messages').push({original: original});
  res.redirect(303, snapshot.ref.toString());
});

export const onCreateUserEmailUppercase =
  functions.auth.user().onCreate(async (user) => {
    return onCreateUser(user);
  });

