export interface UserDbInterface {
  save(): Promise<any>;
}

export class User {
  email: string;
  db: UserDbInterface;

  constructor(email: string, db: UserDbInterface) {
    this.email = email;
    this.db = db;
  }

  save() {
    return this.db.save();
  }
}

export class UserDb implements UserDbInterface {
  save() {
    console.log("testing");
    return Promise.resolve();
  }
}

export const onCreateUser = (user: any) => {
  const rc = new User(user.email, new UserDb());
  return rc.save();
}
